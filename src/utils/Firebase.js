import firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyAvx87trjDg6JhR0it5MHzIrNmdC3Vn0PE",
  authDomain: "musicfy-a4894.firebaseapp.com",
  databaseURL: "https://musicfy-a4894.firebaseio.com",
  projectId: "musicfy-a4894",
  storageBucket: "musicfy-a4894.appspot.com",
  messagingSenderId: "567779637339",
  appId: "1:567779637339:web:0e1893075b387a53cd277e",
};

export default firebase.initializeApp(firebaseConfig);
